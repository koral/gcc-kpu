;; Machine description for Kpu
;; Copyright (C) 2009-2017 Free Software Foundation, Inc.
;; Contributed by Andrea Corallo <andrea_corallo@yahoo.it>

;; This file is part of GCC.

;; GCC is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published
;; by the Free Software Foundation; either version 3, or (at your
;; option) any later version.

;; GCC is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;; License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GCC; see the file COPYING3.  If not see
;; <http://www.gnu.org/licenses/>.

;; -------------------------------------------------------------------------
;; Kpu specific constraints, predicates and attributes
;; -------------------------------------------------------------------------

(include "constraints.md")
(include "predicates.md")

; All instructions are 4 bytes long.
(define_attr "length" "" (const_int 4))

;; -------------------------------------------------------------------------
;; nop instruction
;; -------------------------------------------------------------------------

(define_insn "nop"
  [(const_int 0)]
  ""
  "nop")

;; -------------------------------------------------------------------------
;; Arithmetic instructions
;; -------------------------------------------------------------------------

(define_insn "addsi3"
  [(set (match_operand:SI 0 "register_operand" "=r,r")
        (plus:SI (match_operand:SI 1 "register_operand" "r,r")
                 (match_operand:SI 2 "reg_or_int_operand" "r,i")))]
  ""
  "@
   add\t%0,%1,%2
   addi\t%0,%1,%2")

(define_insn "subsi3"
  [(set (match_operand:SI 0 "register_operand" "=r,r")
        (minus:SI (match_operand:SI 1 "register_operand" "r,r")
                 (match_operand:SI 2 "reg_or_int_operand" "r,i")))]
  ""
  "@
   sub\t%0,%1,%2
   subi\t%0,%1,%2")

(define_insn "mulsi3"
  [(set (match_operand:SI 0 "register_operand" "=r,r")
        (mult:SI (match_operand:SI 1 "register_operand" "r,r")
                 (match_operand:SI 2 "reg_or_int_operand" "r,i")))]
  ""
  "@
   mult\t%0,%1,%2
   multi\t%0,%1,%2")

(define_insn "udivsi3"
  [(set (match_operand:SI 0 "register_operand" "=r,r")
        (div:SI (match_operand:SI 1 "register_operand" "r,r")
                 (match_operand:SI 2 "reg_or_int_operand" "r,i")))]
  ""
  "@
   div\t%0,%1,%2
   divi\t%0,%1,%2")

(define_insn "umodsi3"
  [(set (match_operand:SI 0 "register_operand" "=r,r")
        (div:SI (match_operand:SI 1 "register_operand" "r,r")
                 (match_operand:SI 2 "reg_or_int_operand" "r,i")))]
  ""
  "@
   mod\t%0,%1,%2
   modi\t%0,%1,%2")


(define_insn "lshrsi3"
  [(set (match_operand:SI 0 "register_operand" "=r,r")
        (ashiftrt:SI (match_operand:SI 1 "register_operand" "r,r")
                 (match_operand:SI 2 "reg_or_int_operand" "r,i")))]
  ""
  "@
   shr\t%0,%1,%2
   shri\t%0,%1,%2")

(define_insn "ashlsi3"
  [(set (match_operand:SI 0 "register_operand" "=r,r")
        (ashift:SI (match_operand:SI 1 "register_operand" "r,r")
                 (match_operand:SI 2 "reg_or_int_operand" "r,i")))]
  ""
  "@
   shl\t%0,%1,%2
   shli\t%0,%1,%2")

(define_insn "one_cmplsi2"
  [(set (match_operand:SI 0 "register_operand" "=r")
	(not:SI (match_operand:SI 1 "register_operand" "r")))]
  ""
  "not\t%0,%1")

;; -------------------------------------------------------------------------
;; Logical operators
;; -------------------------------------------------------------------------

(define_insn "andsi3"
  [(set (match_operand:SI 0 "register_operand" "=r,r")
        (and:SI (match_operand:SI 1 "register_operand" "r,r")
                 (match_operand:SI 2 "reg_or_int_operand" "r,i")))]
  ""
  "@
   and\t%0,%1,%2
   andi\t%0,%1,%2")

(define_insn "iorsi3"
  [(set (match_operand:SI 0 "register_operand" "=r,r")
        (ior:SI (match_operand:SI 1 "register_operand" "r,r")
                 (match_operand:SI 2 "reg_or_int_operand" "r,i")))]
  ""
  "@
   or\t%0,%1,%2
   ori\t%0,%1,%2")

(define_insn "xorsi3"
  [(set (match_operand:SI 0 "register_operand" "=r,r")
        (xor:SI (match_operand:SI 1 "register_operand" "r,r")
                 (match_operand:SI 2 "reg_or_int_operand" "r,i")))]
  ""
  "@
   xor\t%0,%1,%2
   xori\t%0,%1,%2")

;; -------------------------------------------------------------------------
;; Move instructions
;; -------------------------------------------------------------------------

;; SImode

(define_expand "movsi"
  [(set (match_operand:SI 0 "nonimmediate_operand" "")
	(match_operand:SI 1 "general_operand" ""))]
  ""
  "
{
  /* If this is a store, force the value into a register.  */
  if (MEM_P (operands[0]))
  {
    operands[1] = force_reg (SImode, operands[1]);
    if (!kpu_move_dest_operand(operands[0], SImode))
      operands[0] = gen_rtx_MEM (SImode, force_reg (SImode, XEXP (operands[0], 0)));
  }
  else {
    if (MEM_P (operands[1])
        && MEM_P (XEXP (operands[1], 0)))
      operands[1] = gen_rtx_MEM (SImode, force_reg (SImode, XEXP (operands[1], 0)));
  }
  gcc_assert(kpu_move_dest_operand(operands[0], SImode));
  gcc_assert(kpu_move_src_operand(operands[1], SImode));
}")

(define_insn "*movsi_internal"
  [(set (match_operand:SI 0 "kpu_move_dest_operand" "=r,r,r,r,B,S,r,R")
	(match_operand:SI 1 "kpu_move_src_operand" "r,i,B,S,r,r,R,r"))]
  "register_operand (operands[0], SImode)
   || register_operand (operands[1], SImode)"
   "@
   mov\t%0,%1
   movi\t%0,%1
   ldw\t%0,%1
   ldaw\t%0,%1
   stw\t%1,%0
   staw\t%1,%0
   ldw\t%0,%1,0
   stw\t%1,%0,0"
)

(define_expand "movhi"
  [(set (match_operand:HI 0 "general_operand" "")
	(match_operand:HI 1 "general_operand" ""))]
  ""
  "
{
  /* If this is a store, force the value into a register.  */
  if (MEM_P (operands[0]))
    operands[1] = force_reg (HImode, operands[1]);
}")

(define_insn "*movhi_internal"
  [(set (match_operand:HI 0 "nonimmediate_operand" "=r,r,r,r,B,S,R,r")
	(match_operand:HI 1 "kpu_move_src_operand" "r,i,B,S,r,r,r,R"))]
  "register_operand (operands[0], HImode)
   || register_operand (operands[1], HImode)"
   "@
   mov\t%0,%1
   movi\t%0,%1
   ldh\t%0,%1
   ldh\t%0,r24,%1
   sth\t%1,%0
   sth\t%1,r24,%0
   sth\t%1,%0,0
   ldh\t%1,%0,0")

(define_expand "movqi"
  [(set (match_operand:QI 0 "general_operand" "")
	(match_operand:QI 1 "general_operand" ""))]
  ""
  "
{
  /* If this is a store, force the value into a register.  */
  if (MEM_P (operands[0]))
    operands[1] = force_reg (QImode, operands[1]);
}")

(define_insn "*movqi_internal"
  [(set (match_operand:QI 0 "nonimmediate_operand" "=r,r,r,r,B,S,R,r")
	(match_operand:QI 1 "kpu_move_src_operand" "r,i,B,S,r,r,r,R"))]
  "register_operand (operands[0], QImode)
   || register_operand (operands[1], QImode)"
   "@
   mov\t%0,%1
   movi\t%0,%1
   ldb\t%0,%1
   ldb\t%0,r24,%1
   stb\t%1,%0
   stb\t%1,r24,%0
   stb\t%1,%0,0
   ldb\t%1,%0,0")

;; -------------------------------------------------------------------------
;; Compare instructions
;; -------------------------------------------------------------------------

(define_constants
  [(CC_REG 30)])

(define_expand "cbranchsi4"
  [(set (reg:CC CC_REG)
        (compare:CC
         (match_operand:SI 1 "general_operand" "")
         (match_operand:SI 2 "general_operand" "")))
   (set (pc)
        (if_then_else (match_operator 0 "kpu_comparison_operator"
                       [(reg:CC CC_REG) (const_int 0)])
                      (label_ref (match_operand 3 "" ""))
                      (pc)))]
  ""
  "
  /* Force the compare operand into registers.  */
  if (GET_CODE (operands[1]) != REG)
	operands[1] = force_reg (SImode, operands[1]);
  if (GET_CODE (operands[2]) != REG && !CONST_INT_P (operands[2]))
	operands[2] = force_reg (SImode, operands[2]);
  ")

(define_insn "*cmpsi1"
  [(set (reg:CC CC_REG)
	(compare
	 (match_operand:SI 0 "register_operand" "r")
	 (match_operand:SI 1 "register_operand"	"r")))]
  ""
  "cmp\\t%0,%1")

(define_insn "*cmpsi2"
  [(set (reg:CC CC_REG)
	(compare
	 (match_operand:SI 0 "register_operand" "r")
	 (match_operand:SI 1 "immediate_operand" "i")))]
  ""
  "cmpi\\t%0,%1")


;; -------------------------------------------------------------------------
;; Branch instructions
;; -------------------------------------------------------------------------

(define_code_iterator cond [gt gtu lt ltu eq ge geu le leu ne])
(define_code_attr CC [(gt "g") (gtu "g") (lt "el") (ltu "el") (eq "eq")
                  (ge "eg") (geu "eg") (le "el") (leu "el") (ne "neq")])

(define_insn "*b<cond:code>"
  [(set (pc)
	(if_then_else (cond (reg:CC CC_REG) (const_int 0))
		      (label_ref (match_operand 0 "" ""))
		      (pc)))]
  ""
  {
    return "b<CC>\\t%l0\n"
    "\tnop";
  }
  [(set_attr "length"	"8")])

;; -------------------------------------------------------------------------
;; Call and Jump instructions
;; -------------------------------------------------------------------------

(define_expand "call"
  [(call (match_operand:SI 0 "memory_operand" "")
		(match_operand 1 "general_operand" ""))]
  ""
{
  gcc_assert (MEM_P (operands[0]));
})

(define_insn "*call"
  [(call (mem:SI (match_operand:SI
		  0 "nonmemory_operand" "i,r"))
	 (match_operand 1 "" ""))]
  ""
  "@
   call\t%0\n\tnop
   mov\tr28,pc\n\tjmpr\t%0\n\tnop"
  [(set_attr "length"	"8,12")])

(define_expand "call_value"
  [(set (match_operand 0 "" "")
		(call (match_operand:SI 1 "memory_operand" "")
		 (match_operand 2 "" "")))]
  ""
{
  gcc_assert (MEM_P (operands[1]));
})

(define_insn "*call_value"
  [(set (match_operand 0 "register_operand" "=r")
	(call (mem:SI (match_operand:SI
		       1 "immediate_operand" "i"))
	      (match_operand 2 "" "")))]
  ""
  "call\t%1\n\tnop"
  [(set_attr "length"	"8")])

(define_insn "*call_value_indirect"
  [(set (match_operand 0 "register_operand" "=r")
	(call (mem:SI (match_operand:SI
		       1 "register_operand" "r"))
	      (match_operand 2 "" "")))]
  ""
  "mov\tr28,pc\n\tjmpr\t%1\n\tnop"
  [(set_attr "length"	"12")])

(define_insn "indirect_jump"
  [(set (pc) (match_operand:SI 0 "nonimmediate_operand" "r"))]
  ""
  "jmpr\t%0\n\tnop"
  [(set_attr "length"	"8")])

(define_insn "jump"
  [(set (pc)
	(label_ref (match_operand 0 "" "")))]
  ""
  "jmp\t%l0\n\tnop"
  [(set_attr "length"	"8")])


;; -------------------------------------------------------------------------
;; Prologue & Epilogue
;; -------------------------------------------------------------------------

(define_expand "prologue"
  [(clobber (const_int 0))]
  ""
  "
{
  kpu_expand_prologue ();
  DONE;
}
")

(define_expand "epilogue"
  [(return)]
  ""
  "
{
  kpu_expand_epilogue ();
  DONE;
}
")

(define_insn "returner"
  [(return)]
  "reload_completed"
  "jmpr\tr28\n\tnop")
