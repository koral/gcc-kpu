;; Constraint definitions for KPU
;; Copyright (C) 2009-2017 Free Software Foundation, Inc.
;; Contributed by Andrea Corallo <andrea_corallo@yahoo.it>

;; This file is part of GCC.

;; GCC is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published
;; by the Free Software Foundation; either version 3, or (at your
;; option) any later version.

;; GCC is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;; License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GCC; see the file COPYING3.  If not see
;; <http://www.gnu.org/licenses/>.

;; -------------------------------------------------------------------------
;; Constraints
;; -------------------------------------------------------------------------

;; Define memory constraints

(define_memory_constraint "S"
  "Memory operand with no base register."
  (and (match_code "mem")
       (match_test "kpu_no_base_memory_operand (op, GET_MODE (op))")))

(define_memory_constraint "B"
  "Memory operand with base register + offset."
  (and (match_code "mem")
       (match_test "kpu_base_off_memory_operand (op, GET_MODE (op))")))

(define_memory_constraint "R"
  "Memory operand register adressed."
  (and (match_code "mem")
       (match_test "kpu_reg_memory_operand (op, GET_MODE (op))")))
